package com.frontend.frontend.controller;

import com.frontend.frontend.FrontendFxmlHessian;
import com.frontend.frontend.model.ClientMedication;
import javafx.application.Platform;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.TimerTask;

public class TimeLogic extends TimerTask {
    public int time;
    public ArrayList<ClientMedication> medications;
    private final PatientUIController patientUIController;
    public TimeLogic(int time, PatientUIController patientUIController) {
        this.time = time;
        this.patientUIController = patientUIController;
        medications = new ArrayList<>();
        medications = FrontendFxmlHessian.getMedications();
      //  System.out.println(medications.toString());
        patientUIController.resetAll(medications);
    }



    public void removeMedication(Long idMedication){
        medications.removeIf(x->{
          return x.getIdMedication() == idMedication;
        });
    }
    public void refreshMedications(){
        medications = FrontendFxmlHessian.getMedications();
    }
    @Override
    public void run() {
        Platform.runLater(()->{
            if(this.time >= 30){
                try {
                  //  System.out.println("intra");
                    FrontendFxmlHessian.endDay();
                    refreshMedications();
                } catch (RemoteException ignored) {
                    //System.out.println("crapa");
                }
                time = 0;
                patientUIController.resetClock();
                patientUIController.resetAll(medications);
            }
        time++;
        for(ClientMedication m: medications){
            if(this.time >= m.getPeriod()){
                try {
                    FrontendFxmlHessian.passedMedicine(m.getName());
                } catch (RemoteException ignored) { }
                m.setPeriod(m.getPeriod() + m.getInitialPeriod());
           /*     for (ClientMedication medication : medications) {
                    System.out.println(medication.getIdMedication());
                    System.out.println(medication.getName());
                    System.out.println(medication.getInitialPeriod());
                    System.out.println(medication.getPeriod());
                    System.out.println();
                }*/
                patientUIController.resetTimer(m);
            }
        }
        patientUIController.changeClock();
        });
    }
}

