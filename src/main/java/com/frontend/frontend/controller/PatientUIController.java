package com.frontend.frontend.controller;

import com.frontend.frontend.FrontendFxmlHessian;
import com.frontend.frontend.model.ClientMedication;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;

public class PatientUIController {

    int clockText;
    TimeLogic timeLogic;
    private final ObservableList<ClientMedication> tvObservableList = FXCollections.observableArrayList();
    @FXML
    public Label clock;
    @FXML
    private TableView<ClientMedication> table;
    @FXML
    public TableColumn<ClientMedication,String> nameColumn;
    @FXML
    void initialize(){
        //asta ii de test. sterge cand ii bine tot
        //tvObservableList.add(MedicationDTO.builder().name("aspirina").period(2).idMedication(1).build());
        clock.setText("0");
        table.setItems(tvObservableList);
        nameColumn.setCellValueFactory(new PropertyValueFactory<ClientMedication,String>("name"));
        timeLogic = new TimeLogic(0,this);
        new Timer().schedule(timeLogic,0,1000);
        addButtonToTable();
    }

    public void addRow(ClientMedication clientMedication){
        table.getItems().add(clientMedication);
    }
    public void deleteRow(Long idMedication){
        table.getItems().removeIf(x->{
            return x.getIdMedication() == idMedication;
        });
    }

    public void resetTimer(ClientMedication clientMedication){
        deleteRow(clientMedication.getIdMedication());
        addRow(clientMedication);
    }

    public void resetAll(ArrayList<ClientMedication> clientMedications){
        tvObservableList.clear();
        tvObservableList.addAll(clientMedications);
        table.setItems(tvObservableList);
    }

    public void changeClock(){
    clockText++;
    setClockText(clockText);
    }

    public void resetClock(){
        clockText = 0;
        setClockText(clockText);
    }

    public void setClockText(long value){
        Date date = new Date(value * 1000);
        clock.setText(String.valueOf(0+ " : " + date.getMinutes() +" : " + date.getSeconds()));
    }

    private void addButtonToTable() {
        TableColumn<ClientMedication, Void> colBtn = new TableColumn("Confirm");

        Callback<TableColumn<ClientMedication, Void>, TableCell<ClientMedication, Void>> cellFactory = new Callback<TableColumn<ClientMedication, Void>, TableCell<ClientMedication, Void>>() {
            @Override
            public TableCell<ClientMedication, Void> call(final TableColumn<ClientMedication, Void> param) {
                final TableCell<ClientMedication, Void> cell = new TableCell<ClientMedication, Void>() {
                    private final Button btn = new Button("ConfirmTaken");
                    {
                        btn.setOnAction((ActionEvent event) -> {
                            ClientMedication clientMedication = getTableView().getItems().get(getIndex());
                           // System.out.println("selectedmedicationDTO: " + medicationDTO.getIdMedication());
                            table.getItems().remove(clientMedication);
                            timeLogic.removeMedication(clientMedication.getIdMedication());
                            try {
                                FrontendFxmlHessian.tookMedicine(clientMedication.getName());
                            } catch (RemoteException e) {
                                System.out.println("crapa1--");
                            }
                        });
                    }
                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };
        colBtn.setCellFactory(cellFactory);
        table.getColumns().add(colBtn);
    }
}

