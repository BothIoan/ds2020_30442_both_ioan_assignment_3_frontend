package com.frontend.frontend;

import javafx.stage.Stage;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class StageInitializer implements ApplicationListener<FrontendFxmlHessian.StageReadyEvent> {
    @Override
    public void onApplicationEvent(FrontendFxmlHessian.StageReadyEvent event) {
        Stage stage = event.getStage();
    }
}
