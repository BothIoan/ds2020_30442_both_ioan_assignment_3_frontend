package com.frontend.frontend;

import com.frontend.frontend.communication.ServerIF;
import com.frontend.frontend.model.ServerMedication;
import com.frontend.frontend.model.ClientMedication;
import com.google.gson.Gson;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ConfigurableApplicationContext;

import java.rmi.RemoteException;
import java.util.*;

import static javafx.application.Application.launch;

public class FrontendFxmlHessian extends Application{
	static ServerIF server;

	public static void endDay() throws RemoteException {
		//System.out.println("intra2");
		Gson gson = new Gson();
		ServerMedication[] serverMedications = gson.fromJson(server.getPlan(), ServerMedication[].class);
		clientMedications = new ArrayList<>();
		for (ServerMedication serverMedication : serverMedications) {
			clientMedications.add(ClientMedication.builder().name(serverMedication.getName()).idMedication(serverMedication.getMedicationId()).period(serverMedication.getInterval().getSeconds()).initialPeriod(serverMedication.getInterval().getSeconds()).build());
		}
		//System.out.println(medicationDTOs.toString());
	}

	public static void tookMedicine(String medicine) throws RemoteException {
		server.takeMedicine(medicine);
	}

	public static void passedMedicine(String medicine) throws RemoteException {
		server.passMedicine(medicine);
	}

	private ConfigurableApplicationContext applicationContext;
	static ArrayList<ClientMedication> clientMedications;
	public static ArrayList<ClientMedication> getMedications(){
		return clientMedications;
	}
	private Stage window;
	@Override
	public void init(){
		applicationContext = new SpringApplicationBuilder(FrontendMain.class).run();
	}
	@Override
	public void start(Stage primaryStage) throws Exception {
		applicationContext.publishEvent(new StageReadyEvent(primaryStage));
		server = applicationContext.getBean(ServerIF.class);
		endDay();
		//System.out.println(clientMedications.toString());
		ClassLoader classLoader = FrontendFxmlHessian.class.getClassLoader();
		Parent root = FXMLLoader.load(Objects.requireNonNull(classLoader.getResource("patientUI.fxml")));
		window = primaryStage;
		Scene mainScene = new Scene(root);
		mainScene.getStylesheets().add("Style.css");
		window.setScene(mainScene);
		window.setTitle("Assignment1");
		window.show();
	}

	@Override
	public void stop(){
		applicationContext.close();
		Platform.exit();
	}

	static class StageReadyEvent extends ApplicationEvent {
		public StageReadyEvent(Stage primaryStage) {
			super(primaryStage);
		}
		public Stage getStage(){
			return ((Stage) getSource());
		}
	}
}

