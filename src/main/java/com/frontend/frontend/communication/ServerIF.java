package com.frontend.frontend.communication;

import java.rmi.RemoteException;

public interface ServerIF  {
    String getPlan() throws RemoteException;
    void takeMedicine(String message) throws RemoteException;
    void passMedicine(String message) throws RemoteException;
}
