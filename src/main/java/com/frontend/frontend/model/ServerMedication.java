package com.frontend.frontend.model;

import lombok.*;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.Duration;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class ServerMedication implements Serializable {
    private Long medicationId;
    private String name;
    private Duration interval;



}