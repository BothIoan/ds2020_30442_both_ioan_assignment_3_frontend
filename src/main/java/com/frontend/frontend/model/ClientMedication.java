package com.frontend.frontend.model;

import javafx.scene.control.MenuItem;
import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class ClientMedication extends MenuItem {
    String name;
    Long period;
    Long idMedication;
    Long initialPeriod;
}
