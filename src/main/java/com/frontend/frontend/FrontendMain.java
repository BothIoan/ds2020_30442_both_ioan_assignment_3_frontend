package com.frontend.frontend;
import javafx.application.Application;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.rmi.RemoteException;

@SpringBootApplication
public class FrontendMain {
    public static void main(String[] args) throws RemoteException {
		//ServerIF server =  SpringApplication.run(HessianclientApplication.class, args).getBean(ServerIF.class);
        Application.launch(FrontendFxmlHessian.class,args);
		//System.out.println(server.getPlan());
	}
}
